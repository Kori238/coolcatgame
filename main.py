from kivy.config import Config
Config.set('graphics', 'width', '900')
Config.set('graphics', 'height', '600') # Config to disable resizing and to set the window size
Config.set('graphics', 'resizable', False)
Config.set('kivy', 'log_level', 'debug')
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.image import Image
from kivy.lang.builder import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.graphics import Rectangle
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.properties import NumericProperty, StringProperty
from kivy.uix.screenmanager import Screen, ScreenManager, SlideTransition

cat = Builder.load_file('catpop.kv') # Loads the kivy language files
Builder.load_file('coolcat.kv')
tick_speed = last_tick_speed = 1
thirst = hunger = score = 0 # Variable assignment
happiness = cleanliness = 100
game = layout = tick_clock = tick_increase = None
first_loop = True
class GameTick(object): # This class holds the code for each tick of the game
    def start(self):
        global tick_speed, tick_clock
        tick_clock = Clock.schedule_interval(self.pop, tick_speed) # Schedules a repeating event

    def increase(self, dt): # Increases the speed of the game by reducing the value of the tick speed
        global tick_speed, score
        tick_speed -= tick_speed / 25
        score += 1

    def pop(self, dt): # This is called every game tick
        global first_loop, tick_speed, last_tick_speed, tick_clock, game, thirst, hunger, happiness, cleanliness, layout, cat, score, tick_increase
        if sm.current_screen.name != 'gamescreen': # This will ensure the game pauses when the user is on any screen other than the game screen
            first_loop = True
            try:
                Clock.unschedule(tick_increase)
            except:
                pass
            return
        elif first_loop: # Schedules the tick speed increase event
            tick_increase = Clock.schedule_interval(self.increase, 1)
            first_loop = False
        game.thirst_label = thirst = thirst + 1 # Depletes the stat meters and updates labels
        game.hunger_label = hunger = hunger + 1
        game.happiness_label = happiness = happiness - 1 
        game.cleanliness_label = cleanliness = cleanliness - 1
        game.tick_speed_label = tick_speed
        if happiness > 100 or cleanliness > 100 or hunger > 100 or thirst > 100 or happiness < 0 or cleanliness < 0 or hunger < 0 or thirst < 0:
            Clock.unschedule(tick_clock)
            Clock.unschedule(tick_speed)
            self.gameover(dt)
        if tick_speed != last_tick_speed:
            Clock.unschedule(tick_clock) # Updates the tick speed on the clock
            tick_clock = Clock.schedule_interval(self.pop, tick_speed)
            last_tick_speed = tick_speed
            print("[INFO   ] [Game        ] Tick speed has been updated to: " + str(tick_speed)) # Displays tick speed to console
        if cat.source[21:] == "ClosedSmall.png": # Changes the cat image to make him 'pop'
            cat.source = "resources/cats/CatPopOpenSmall.png"
        else:
            cat.source = "resources/cats/CatPopClosedSmall.png"

    def gameover(self, dt): # This is run when the game ends to display end credits to the user
        global sc, score, hunger, thirst, happiness, cleanliness, hunger, outro_screen
        message = ''
        if hunger > 100:
            message += "Starvation, "
        elif hunger < 0:
            message += "Over Feeding, "
        if thirst > 100:
            message += "Dehydration, "
        elif thirst < 0:
            message += "Drowning, "
        if happiness > 100:
            message += "Joy, "
        elif happiness < 0:
            message += "Depression, "
        if cleanliness > 100:
            message += "Too Clean, "
        elif cleanliness < 0:
            message += "Too Dirty, "
        message = message[:-2]
        outro_screen.causes = message # Updates the lablels
        outro_screen.score = score
        sm.current = 'outroscreen' # Changes the screen to the outro screen
class CoolCatGame(Widget):
    thirst_label = NumericProperty(thirst)
    hunger_label = NumericProperty(hunger)
    happiness_label = NumericProperty(happiness) # Initialises kivy variables
    cleanliness_label = NumericProperty(cleanliness)
    tick_speed_label = NumericProperty(tick_speed)
    tick = GameTick() # Starts the clock
    tick.start()
    def feed(self, dt):
        global game, hunger
        game.hunger_label = hunger = hunger - 10 # Decreases hunger by 10
    def water(self, dt):
        global game, thirst
        game.thirst_label = thirst = thirst - 10 # Decreases thirst by 10
    def forefit(self, dt):
        global sm, tick_clock, tick_speed # Ends the game early
        Clock.unschedule(tick_clock)
        Clock.unschedule(tick_speed)
        GameTick.gameover(self, dt)
    def main_menu(self, dt): # Returns the user to the main menu
        global sm
        sm.transition = SlideTransition(direction='right') # Changes the transition to right so it looks like you are going back to the main menu
        sm.current = 'introscreen'
        sm.transition = SlideTransition(direction='left')
    def play(self, dt): # Increases happiness by 10
        global game, happiness
        game.happiness_label = happiness = happiness + 10
    def wash(self, dt): # Increases cleanliness by 10
        global game, cleanliness
        game.cleanliness_label = cleanliness = cleanliness + 10
class GameScreen(Screen): # Screen Classes to hold widgets from the kivy files
    pass
class OutroScreen(Screen):
    score = NumericProperty(0) # Sets variables for the label so they can be changed later
    causes = StringProperty('')
class IntroScreen(Screen):
    pass

Builder.load_file('screens.kv')

sm = ScreenManager() # Screen Manager - used to hold different screens that I can swap between
game = CoolCatGame()
layout = FloatLayout(size=(600, 900)) # A layout with no restrictions equal to the window size
grid = GridLayout(cols=3, rows=2, padding=[0,400,0,0]) # A grid layout to hold the buttons with 400p padding at the top
grid.add_widget(Button(text='Give Food', on_press=game.feed)) # Add buttons with the respective functions
grid.add_widget(Button(text='Forefit', on_press=game.forefit))
grid.add_widget(Button(text='Play With', on_press=game.play))
grid.add_widget(Button(text='Give Water', on_press=game.water))
grid.add_widget(Button(text='Menu/Info', on_press=game.main_menu))
grid.add_widget(Button(text='Wash', on_press =game.wash))
layout.add_widget(grid)
layout.add_widget(game, 0) # Add the widgets with priority so the cat displays underneath the stat meters
layout.add_widget(cat, 1)

game_screen = GameScreen(name='gamescreen')
game_screen.add_widget(layout)
sm.add_widget(IntroScreen(name='introscreen')) # Adds the screens to the screen manager
sm.add_widget(game_screen)
outro_screen = OutroScreen(name='outroscreen')
sm.add_widget(outro_screen)
sm.current = 'introscreen'
class CoolCatApp(App, Screen):
    def build(self): # Builds the screens
        return sm

if __name__ == '__main__':
    CoolCatApp().run()

